# OpenML dataset: CovPokElec

https://www.openml.org/d/149

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Albert Bifet  
**Source**: [MOA](http://moa.cms.waikato.ac.nz/datasets/) - 2009  
**Please cite**:   

Dataset created to study concept drift in stream mining. It is constructed by combining the Covertype, Poker-Hand, and Electricity datasets. More details can be found in:
Albert Bifet, Geoff Holmes, Bernhard Pfahringer, Richard Kirkby, and Ricard Gavaldà. 2009. New ensemble methods for evolving data streams. In Proceedings of the 15th ACM SIGKDD international conference on Knowledge discovery and data mining (KDD '09).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/149) of an [OpenML dataset](https://www.openml.org/d/149). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/149/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/149/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/149/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

